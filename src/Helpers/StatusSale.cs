using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace src.Helpers
{
    public static class StatusSale
    {
        public const string AGUARDANDO_PAGAMENTO = "Aguardando pagamento";
        public const string PAGAMENTO_APROVADO = "Pagamento Aprovado";
        public const string CANCELADA = "Cancelada";

        public const string ENVIADO_PARA_TRANSPORTADORA = "Em transporte";
        public const string ENTREGUE = "Entregue";
    }
}