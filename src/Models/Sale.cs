using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using src.Helpers;

namespace src.Models
{
    public class Sale
    {
        public Sale()
        {
            this.Status = StatusSale.AGUARDANDO_PAGAMENTO;
            this.Data = DateTime.Now;
            this.Items = new List<SaleItem>();
        }
        public int Id { get; set; }
        public DateTime Data { get; set; }
        public string Status { get; set; }
        public int? SellerId { get; set; }
        public List<SaleItem> Items { get; set; }

    }

    public class SaleRequest
    {
        public int? SellerId { get; set; }
        public List<Items> Items { get; set; }
    }

}