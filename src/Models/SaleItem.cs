using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace src.Models
{
    public class SaleItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class ItemRequest
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class Items
    {
        public int Id { get; set; }
    }
}