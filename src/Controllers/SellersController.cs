using System.Net;
using Microsoft.AspNetCore.Mvc;

using src.Context;
using src.Models;

namespace src.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SellersController : ControllerBase
    {
        private SalesContext _context { get; set; }

        public SellersController(SalesContext context)
        {
            this._context = context;
        }

        [HttpGet]
        public ActionResult<List<Seller>> Get()
        {
            var result = _context.Sellers.ToList();
            if (!result.Any())
            {
                return NoContent();
            }
            return Ok(result);
        }

        [HttpPost]
        public ActionResult<Seller> CreateSeller([FromBody] SellerRequest seller)
        {

            var newSeller = new Seller
            {
                CPF = seller.CPF,
                Name = seller.Name,
                Email = seller.Email,
                PhoneNumber = seller.PhoneNumber

            };

            _context.Sellers.Add(newSeller);
            _context.SaveChanges();

            return Created("Created", newSeller);
        }

        [HttpGet("{id}")]
        public ActionResult<Seller> GetSale(int id)
        {

            var result = _context.Sellers.SingleOrDefault(e => e.Id == id);

            if (result is null)
            {
                return NotFound(new
                {
                    message = "Seller not found!",
                    status = HttpStatusCode.NotFound
                });
            }

            return Ok(result);
        }

        [HttpPut("{id}")]
        public ActionResult<Object> Update([FromRoute] int id, [FromBody] SellerRequest seller)
        {

            var result = _context.Sellers.SingleOrDefault(e => e.Id == id);

            if (result is null)
            {
                return NotFound(new
                {
                    message = "Seller not found!",
                    status = HttpStatusCode.NotFound
                });
            }

            foreach (var property in seller.GetType().GetProperties())
            {
                var resultProperty = result.GetType().GetProperty(property.Name);
                if (resultProperty != null && resultProperty.CanWrite && property.GetValue(seller) != null)
                {
                    resultProperty.SetValue(result, property.GetValue(seller));
                }
            }

            try
            {
                _context.SaveChanges();
            }
            catch (System.Exception)
            {
                return BadRequest(new
                {
                    message = $"Seller with id {id} not updated",
                    status = HttpStatusCode.BadRequest
                });
            }

            return Ok(new
            {
                message = $"Seller with id {id} updated",
                status = HttpStatusCode.OK
            });
        }

        [HttpDelete("{id}")]
        public ActionResult<Object> Delete([FromRoute] int id)
        {
            var result = _context.Sellers.SingleOrDefault(e => e.Id == id);
            if (result is null)
            {
                return BadRequest(new
                {
                    message = "invalid request, content not found",
                    status = HttpStatusCode.BadRequest
                });
            }
            _context.Sellers.Remove(result);
            _context.SaveChanges();

            return Ok(new
            {
                message = $"Seller deleted with id {id}",
                status = HttpStatusCode.OK
            });
        }
    }
}