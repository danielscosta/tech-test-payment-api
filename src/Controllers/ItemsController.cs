using System.Net;
using Microsoft.AspNetCore.Mvc;

using src.Context;
using src.Models;

namespace src.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ItemsController : ControllerBase
    {
        private SalesContext _context { get; set; }

        public ItemsController(SalesContext context)
        {
            this._context = context;
        }


        [HttpGet]
        public ActionResult<List<SaleItem>> Get()
        {
            var result = _context.Items.ToList();
            if (!result.Any())
            {
                return NoContent();
            }
            return Ok(result);
        }

        [HttpPost]
        public ActionResult<SaleItem> CreateSaleItem([FromBody] ItemRequest item)
        {

            var newItem = new SaleItem
            {
                Name = item.Name,
                Value = item.Value
            };

            _context.Items.Add(newItem);
            _context.SaveChanges();

            return Created("Created", newItem);
        }

        [HttpGet("{id}")]
        public ActionResult<SaleItem> GetSale(int id)
        {

            var result = _context.Items.SingleOrDefault(e => e.Id == id);

            if (result is null)
            {
                return NotFound(new
                {
                    message = "Item not found!",
                    status = HttpStatusCode.NotFound
                });
            }

            return Ok(result);
        }

        [HttpPut("{id}")]
        public ActionResult<Object> Update([FromRoute] int id, [FromBody] ItemRequest item)
        {

            var result = _context.Items.SingleOrDefault(e => e.Id == id);

            if (result is null)
            {
                return NotFound(new
                {
                    message = "Item not found!",
                    status = HttpStatusCode.NotFound
                });
            }

            foreach (var property in item.GetType().GetProperties())
            {
                var resultProperty = result.GetType().GetProperty(property.Name);
                if (resultProperty != null && resultProperty.CanWrite && property.GetValue(item) != null)
                {
                    resultProperty.SetValue(result, property.GetValue(item));
                }
            }

            try
            {
                _context.SaveChanges();
            }
            catch (System.Exception)
            {
                return BadRequest(new
                {
                    message = $"Item with id {id} not updated",
                    status = HttpStatusCode.BadRequest
                });
            }

            return Ok(new
            {
                message = $"Item with id {id} updated",
                status = HttpStatusCode.OK
            });
        }

        [HttpDelete("{id}")]
        public ActionResult<Object> Delete([FromRoute] int id)
        {
            var result = _context.Items.SingleOrDefault(e => e.Id == id);
            if (result is null)
            {
                return BadRequest(new
                {
                    message = "invalid request, content not found",
                    status = HttpStatusCode.BadRequest
                });
            }
            _context.Items.Remove(result);
            _context.SaveChanges();

            return Ok(new
            {
                message = $"Item deleted with id {id}",
                status = HttpStatusCode.OK
            });
        }
    }
}