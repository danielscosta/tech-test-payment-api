using System.Net;
using Microsoft.AspNetCore.Mvc;

using src.Models;
using src.Context;
using Microsoft.EntityFrameworkCore;
using src.Helpers;

namespace src.Controllers

{
    [ApiController]
    [Route("[controller]")]
    public class SalesController : ControllerBase
    {
        private SalesContext _context { get; set; }

        public SalesController(SalesContext context)
        {
            this._context = context;
        }

        [HttpGet]
        public ActionResult<List<Sale>> Get()
        {
            var result = _context.Sales.Include(p => p.Items).ToList();
            if (!result.Any())
            {
                return NoContent();
            }
            return Ok(result);
        }

        [HttpPost]
        public ActionResult<Sale> CreateSale([FromBody] SaleRequest sale)
        {
            if (sale == null || sale.SellerId == null || sale.Items == null || sale.Items.Count == 0)
            {
                return BadRequest("Sale, Seller and SaleItems are required.");
            }

            List<SaleItem> items = new List<SaleItem>();

            for (var i = 0; i < sale.Items.Count; i++)
            {
                var id = sale.Items[i].Id;

                var item = _context.Items.SingleOrDefault(e => e.Id == id);

                if (item != null)
                {
                    items.Add(item);
                }
            }

            var newSale = new Sale
            {
                Status = StatusSale.AGUARDANDO_PAGAMENTO,
                Data = DateTime.Now,
                SellerId = sale.SellerId,
                Items = items
            };

            _context.Sales.Add(newSale);
            _context.SaveChanges();

            return Created("Created", newSale);
        }

        [HttpGet("{id}")]
        public ActionResult<Sale> GetSale(int id)
        {

            var result = _context.Sales.SingleOrDefault(e => e.Id == id);
            //.Include(p => p.Items)
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }
    }
}
/*
[HttpPut("{id}")]
public IActionResult UpdateSale(int id, [FromBody] Status newStatus)
{
    Sale sale = sales.FirstOrDefault(s => s.Id == id);

    if (sale == null)
    {
        return NotFound();
    }

    switch (sale.Status)
    {
        case SaleStatus.AguardandoPagamento:
            if (newStatus != SaleStatus.PagamentoAprovado && newStatus != SaleStatus.Cancelada)
            {
                return BadRequest("The new status is not allowed.");
            }
            break;
        case SaleStatus.PagamentoAprovado:
            if (newStatus != SaleStatus.EnviadoParaTransportadora && newStatus != SaleStatus.Cancelada)
            {
                return BadRequest("The new status is not allowed.");
            }
            break;
        case SaleStatus.EnviadoParaTransportadora:
            if (newStatus != SaleStatus.Entregue)
            {
                return BadRequest("The new status is not allowed.");
            }
            break;
        case SaleStatus.Entregue:
        case SaleStatus.Cancelada:
            return BadRequest("The sale status cannot be changed.");
    }

    sale.Status = newStatus;

    return Ok(sale);

}*/
